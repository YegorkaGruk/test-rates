package com.yegor.gruk.ratestest.ui.activity.main.adapter.listener

interface RatesAdapterItemTextChangeListener {
    fun onItemTextChanged(text: String)
}