package com.yegor.gruk.ratestest.ui.activity.main.adapter

import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yegor.gruk.ratestest.R
import com.yegor.gruk.ratestest.ui.activity.main.adapter.item.FocusChangePayload
import com.yegor.gruk.ratestest.ui.activity.main.adapter.item.InputAmountChangedPayload
import com.yegor.gruk.ratestest.ui.activity.main.adapter.item.RateChangedPayload
import com.yegor.gruk.ratestest.ui.activity.main.adapter.item.RateItem
import com.yegor.gruk.ratestest.ui.activity.main.adapter.listener.RatesAdapterItemClickListener
import com.yegor.gruk.ratestest.ui.activity.main.adapter.listener.RatesAdapterItemMovedListener
import com.yegor.gruk.ratestest.ui.activity.main.adapter.listener.RatesAdapterItemTextChangeListener

class RatesAdapter(
    private val adapterItemMoved: RatesAdapterItemMovedListener,
    private val handler: Handler
) : RecyclerView.Adapter<RatesViewHolder>(),
    RatesAdapterItemClickListener,
    RatesAdapterItemTextChangeListener {

    companion object {
        private const val DEFAULT_RELATIVE_RATE = 1f
        private const val DEFAULT_TOP_POSITION = 0
    }

    private val ratesList: ArrayList<RateItem> = arrayListOf()
    private val ratesIndexes: ArrayList<Int> = arrayListOf()

    private val textWatcherAdapter = RatesTextWatcher(this)
    private val spaceRegex = "\\s".toRegex()

    private var rateMultiplier = DEFAULT_RELATIVE_RATE

    override fun onItemClicked(position: Int) {
        if (position != DEFAULT_TOP_POSITION) {
            movedToTopItemList(position, DEFAULT_TOP_POSITION)
            setFocusChangePayload(position, false)
            setFocusChangePayload(DEFAULT_TOP_POSITION, true)
            adapterItemMoved.onItemMoved(DEFAULT_TOP_POSITION)
        }
    }

    override fun onItemTextChanged(text: String) {
        val ratesChangedIndex = ratesIndexes[DEFAULT_TOP_POSITION]
        val rateItem = ratesList[ratesChangedIndex]
        val normalized = text.replace(spaceRegex, "")
        val amount = normalized.toFloatOrNull() ?: 0f
        rateMultiplier = amount / rateItem.rate
        setRelativeRate(rateMultiplier)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_currency, parent, false)
        return RatesViewHolder(view)
    }

    override fun onBindViewHolder(holder: RatesViewHolder, position: Int) {}

    override fun onBindViewHolder(
        holder: RatesViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        val ratesIndex = ratesIndexes[position]
        val item = ratesList[ratesIndex]
        if (payloads.size == 0) {
            holder.bind(item, rateMultiplier, this, textWatcherAdapter)
            return
        }
        val payload = payloads[0]
        when {
            payload is FocusChangePayload -> {
                holder.updateFocus(payload.hasFocus, textWatcherAdapter)
            }
            payload is InputAmountChangedPayload && position != DEFAULT_TOP_POSITION -> {
                holder.updateRate(item, payload.relativeRate, textWatcherAdapter)
            }
            payload is RateChangedPayload && position != DEFAULT_TOP_POSITION && payload.changes[ratesIndex] -> {
                holder.updateRate(item, rateMultiplier, textWatcherAdapter)
            }
        }
    }

    override fun getItemCount() = ratesList.size

    fun updateCurrencies(newRates: List<RateItem>) {
        if (this.ratesList.size == 0) {
            ratesList.addAll(newRates)
            ratesIndexes.addAll(newRates.indices)
            notifyDataSetChanged()
        } else {

            updateRateMultiplier(newRates)

            val rateChangedPayload = rateChangedPayload(newRates)

            ratesList.clear()
            ratesList.addAll(newRates)
            notifyItemRangeChanged(0, itemCount, rateChangedPayload)
        }
    }

    private fun rateChangedPayload(newRates: List<RateItem>): RateChangedPayload {
        val changes = mutableListOf<Boolean>()
        newRates.forEachIndexed { index, rateItem -> changes.add(rateItem.rate != ratesList[index].rate) }
        return RateChangedPayload(changes)
    }

    private fun movedToTopItemList(from: Int, to: Int) {
        val fromIndex = ratesIndexes[from]
        for (index in from downTo to + 1) {
            ratesIndexes[index] = ratesIndexes[index - 1]
        }
        ratesIndexes[to] = fromIndex
        notifyItemMoved(from, to)
    }

    private fun setRelativeRate(relativeRate: Float) {
        handler.post {
            notifyItemRangeChanged(
                DEFAULT_TOP_POSITION + 1,
                itemCount,
                InputAmountChangedPayload(relativeRate)
            )
        }
    }

    private fun setFocusChangePayload(position: Int, hasFocus: Boolean) {
        notifyItemChanged(position, FocusChangePayload(hasFocus))
    }

    private fun updateRateMultiplier(newRates: List<RateItem>) {
        val oldTopIndex = ratesIndexes[DEFAULT_TOP_POSITION]
        val oldRate = ratesList[oldTopIndex].rate
        val newRate = newRates[oldTopIndex].rate
        rateMultiplier = rateMultiplier * oldRate / newRate
    }
}