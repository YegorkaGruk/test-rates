package com.yegor.gruk.ratestest.ui.activity.main.adapter.item

data class RateChangedPayload(val changes: List<Boolean>)