package com.yegor.gruk.ratestest.injection

import android.app.Application
import com.yegor.gruk.ratestest.RatesApp
import com.yegor.gruk.ratestest.injection.module.ApplicationModule
import com.yegor.gruk.ratestest.injection.module.DataModule
import com.yegor.gruk.ratestest.injection.module.DomainModule
import com.yegor.gruk.ratestest.injection.module.PresentationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApplicationModule::class,
        DataModule::class,
        DomainModule::class,
        PresentationModule::class
    ]
)
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: RatesApp)
}
