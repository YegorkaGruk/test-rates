package com.yegor.gruk.ratestest.ui.activity.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.yegor.gruk.domain.usecase.load_currencies.LoadCurrenciesState
import com.yegor.gruk.domain.usecase.load_currencies.LoadCurrenciesUseCase
import com.yegor.gruk.ratestest.ui.activity.base.BaseViewModel
import io.reactivex.disposables.Disposable

class MainActivityViewModel(private val loadCurrencies: LoadCurrenciesUseCase) : BaseViewModel() {

    private val loadCurrenciesLiveDataInternal = MutableLiveData<LoadCurrenciesState>()

    val loadCurrenciesLiveData: LiveData<LoadCurrenciesState> = loadCurrenciesLiveDataInternal

    private var disposable: Disposable? = null

    override fun onStart() {
        super.onStart()
        loadCurrencies()
    }

    override fun onStop() {
        super.onStop()
        disposable?.dispose()
    }

    fun loadCurrencies() {
        val disposable = loadCurrencies.execute(null)
            .doOnSubscribe { loadCurrenciesLiveDataInternal.postValue(LoadCurrenciesState.StateProcessing) }
            .subscribe(
                { loadCurrenciesLiveDataInternal.value = LoadCurrenciesState.StateSuccess(it) },
                { loadCurrenciesLiveDataInternal.value = LoadCurrenciesState.StateError(it) }
            )
        this.disposable = disposable
    }
}