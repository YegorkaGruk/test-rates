package com.yegor.gruk.ratestest.ui.activity.main.adapter.listener

interface RatesAdapterItemClickListener {
    fun onItemClicked(position: Int)
}