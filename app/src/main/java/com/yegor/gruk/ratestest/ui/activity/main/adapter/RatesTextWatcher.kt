package com.yegor.gruk.ratestest.ui.activity.main.adapter

import com.yegor.gruk.ratestest.ui.activity.main.adapter.listener.RatesAdapterItemTextChangeListener
import com.yegor.gruk.ratestest.ui.common.TextWatcherAdapter

class RatesTextWatcher(private val listener: RatesAdapterItemTextChangeListener) :
    TextWatcherAdapter {

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        listener.onItemTextChanged(s.toString())
    }
}