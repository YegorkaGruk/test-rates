package com.yegor.gruk.ratestest.injection.module

import com.yegor.gruk.ratestest.ui.activity.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PresentationModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}