package com.yegor.gruk.ratestest.ui.activity.main.adapter.item

data class InputAmountChangedPayload(val relativeRate: Float)