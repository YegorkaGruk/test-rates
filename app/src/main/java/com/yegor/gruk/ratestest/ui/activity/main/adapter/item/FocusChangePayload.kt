package com.yegor.gruk.ratestest.ui.activity.main.adapter.item

data class FocusChangePayload(var hasFocus: Boolean)