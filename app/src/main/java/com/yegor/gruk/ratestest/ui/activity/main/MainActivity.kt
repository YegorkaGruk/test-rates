package com.yegor.gruk.ratestest.ui.activity.main

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.yegor.gruk.domain.usecase.load_currencies.LoadCurrenciesState
import com.yegor.gruk.ratestest.R
import com.yegor.gruk.ratestest.ui.activity.base.BaseViewModelActivity
import com.yegor.gruk.ratestest.ui.activity.main.adapter.RatesAdapter
import com.yegor.gruk.ratestest.ui.activity.main.adapter.item.RateItem
import com.yegor.gruk.ratestest.ui.activity.main.adapter.listener.RatesAdapterItemMovedListener
import com.yegor.gruk.ratestest.ui.common.PreCachingLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : BaseViewModelActivity<MainActivityViewModel>(), RatesAdapterItemMovedListener {

    companion object {
        private const val OFFSET_COUNT = 5
    }

    @Inject
    lateinit var vmFactory: MainActivityViewModelFactory

    private val ratesAdapter: RatesAdapter by lazy { RatesAdapter(this, getHandler()) }

    override fun getViewModelClass() = MainActivityViewModel::class.java

    override fun getViewModelFactory(): ViewModelProvider.Factory = vmFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val preCachingLayoutManager = PreCachingLayoutManager(this, getExtraLayoutSpace())
        preCachingLayoutManager.isItemPrefetchEnabled = true
        rates_list.setHasFixedSize(true)
        rates_list.layoutManager = preCachingLayoutManager
        rates_list.adapter = ratesAdapter

        error_button_reload.setOnClickListener { reload() }

        viewModel.loadCurrenciesLiveData.observe(this, Observer<LoadCurrenciesState> { event ->
            if (event is LoadCurrenciesState.StateSuccess) {
                val currencies = mapCurrencies(event)
                ratesAdapter.updateCurrencies(currencies)
            } else if (event is LoadCurrenciesState.StateError) {
                error_message.text = getString(R.string.exception_text, event.error.message)
            }
            setUpControls(event)
        })
    }

    override fun onItemMoved(position: Int) {
        rates_list.layoutManager?.scrollToPosition(position)
    }

    private fun getExtraLayoutSpace(): Int {
        return (resources.getDimension(R.dimen.rates_title_start_margin) * OFFSET_COUNT).toInt()
    }

    private fun reload() {
        viewModel.loadCurrencies()
    }

    private fun setUpControls(loadCurrenciesState: LoadCurrenciesState) {
        when (loadCurrenciesState) {
            is LoadCurrenciesState.StateProcessing -> {
                showProgress()
            }
            is LoadCurrenciesState.StateSuccess -> {

                rates_list.visibility = View.VISIBLE
                error_message.visibility = View.GONE
                error_button_reload.visibility = View.GONE

                hideProgress()
            }
            is LoadCurrenciesState.StateError -> {

                rates_list.visibility = View.GONE
                error_message.visibility = View.VISIBLE
                error_button_reload.visibility = View.VISIBLE

                hideProgress()
            }
        }
    }

    private fun mapCurrencies(event: LoadCurrenciesState.StateSuccess): List<RateItem> {
        return event.result.rates.map { rateEntity ->
            RateItem(
                rateEntity.code,
                rateEntity.description,
                rateEntity.iconUri,
                rateEntity.rate
            )
        }
    }

    private fun getHandler() = Handler(Looper.getMainLooper())
}
