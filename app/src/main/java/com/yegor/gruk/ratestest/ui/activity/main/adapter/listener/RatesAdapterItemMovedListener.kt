package com.yegor.gruk.ratestest.ui.activity.main.adapter.listener

interface RatesAdapterItemMovedListener {
    fun onItemMoved(position: Int)
}