package com.yegor.gruk.ratestest.ui.activity.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yegor.gruk.domain.usecase.load_currencies.LoadCurrenciesUseCase
import javax.inject.Inject

class MainActivityViewModelFactory @Inject constructor(
    private val loadCurrencies: LoadCurrenciesUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            MainActivityViewModel(
                loadCurrencies
            ) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}