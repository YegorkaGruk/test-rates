package com.yegor.gruk.ratestest.injection.module

import android.content.Context
import com.yegor.gruk.data.repository.CurrenciesRepositoryImpl
import com.yegor.gruk.data.service.rest.APIServiceFactory
import com.yegor.gruk.data.service.rest.RatesService
import com.yegor.gruk.data.service.storage.AssetsService
import com.yegor.gruk.data.service.storage.AssetsServiceImpl
import com.yegor.gruk.data.store.currency_image.CurrencyImageStore
import com.yegor.gruk.data.store.currency_image.CurrencyImageStoreImpl
import com.yegor.gruk.data.store.currency_names.CurrencyNamesStore
import com.yegor.gruk.data.store.currency_names.CurrencyNamesStoreImpl
import com.yegor.gruk.data.store.rate_api.RateApiStore
import com.yegor.gruk.data.store.rate_api.RateApiStoreImpl
import com.yegor.gruk.domain.repository.CurrenciesRepository
import com.yegor.gruk.ratestest.BuildConfig
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
abstract class DataModule {

    @Module
    companion object {
        @Provides
        @JvmStatic
        @Singleton
        fun provideRatesService(): RatesService {
            return APIServiceFactory.makeRestService(BuildConfig.DEBUG)
        }

        @Provides
        @JvmStatic
        @Singleton
        fun provideAssetsService(context: Context): AssetsService {
            return AssetsServiceImpl(context)
        }
    }

    @Binds
    @Singleton
    abstract fun bindCurrencyImageStore(store: CurrencyImageStoreImpl): CurrencyImageStore

    @Binds
    @Singleton
    abstract fun bindRateApiStore(store: RateApiStoreImpl): RateApiStore

    @Binds
    @Singleton
    abstract fun bindCurrencyNamesStore(store: CurrencyNamesStoreImpl): CurrencyNamesStore

    @Binds
    @Singleton
    abstract fun bindCurrenciesRepository(repository: CurrenciesRepositoryImpl): CurrenciesRepository
}