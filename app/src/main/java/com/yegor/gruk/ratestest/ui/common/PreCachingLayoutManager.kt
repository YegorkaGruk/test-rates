package com.yegor.gruk.ratestest.ui.common

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class PreCachingLayoutManager : LinearLayoutManager {

    private val defaultExtraLayoutSpace = 600

    private var extraLayoutSpace = -1

    constructor(context: Context, extraLayoutSpace: Int) : super(context) {
        this.extraLayoutSpace = extraLayoutSpace
    }

    fun setExtraLayoutSpace(extraLayoutSpace: Int) {
        this.extraLayoutSpace = extraLayoutSpace
    }

    override fun getExtraLayoutSpace(state: RecyclerView.State): Int {
        return if (extraLayoutSpace > 0) extraLayoutSpace else defaultExtraLayoutSpace
    }
}