package com.yegor.gruk.ratestest.ui.activity.base

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.yegor.gruk.ratestest.ui.dialog.ProgressDialogFragment
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseViewModelActivity<VM : BaseViewModel> : DaggerAppCompatActivity() {

    protected lateinit var viewModel: VM

    private var progressDialog: ProgressDialogFragment? = null

    protected abstract fun getViewModelClass(): Class<VM>

    protected abstract fun getViewModelFactory(): ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, getViewModelFactory())[(getViewModelClass())]
        lifecycle.addObserver(viewModel)
    }

    override fun onStop() {
        super.onStop()
        hideProgress()
    }

    protected fun showProgress() {
        hideProgress()
        val fm = supportFragmentManager
        val fragment = fm.findFragmentByTag(ProgressDialogFragment::class.java.simpleName)
        val ft = fm.beginTransaction()
        if (fragment != null) {
            ft.remove(fragment)
        }
        val progressDialog = ProgressDialogFragment()
        ft.add(progressDialog, ProgressDialogFragment::class.java.simpleName)
        ft.commitAllowingStateLoss()
        this.progressDialog = progressDialog
    }

    protected fun hideProgress() {
        progressDialog?.dismissAllowingStateLoss()
    }
}