package com.yegor.gruk.ratestest.ui.activity.main.adapter.item

data class RateItem(
    val code: String,
    val description: String,
    val iconUrl: String,
    var rate: Float
)