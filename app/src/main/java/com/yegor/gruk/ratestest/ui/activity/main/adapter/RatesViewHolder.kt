package com.yegor.gruk.ratestest.ui.activity.main.adapter

import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.yegor.gruk.ratestest.R
import com.yegor.gruk.ratestest.ui.activity.main.adapter.item.RateItem
import com.yegor.gruk.ratestest.ui.activity.main.adapter.listener.RatesAdapterItemClickListener
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class RatesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    companion object {
        private const val DOT = '.'
        private const val REGULAR_FORMAT_PATTERN = "#,##0.##"
        private const val EXPONENTIAL_FORMAT_PATTERN = "000.E0"
        private const val EXPONENTIAL_FORMAT_EDGE = 0.01f
    }

    private val icon: ImageView = itemView.findViewById(R.id.currency_icon)
    private val code: TextView = itemView.findViewById(R.id.currency_code)
    private val description: TextView = itemView.findViewById(R.id.currency_description)
    private val rate: EditText = itemView.findViewById(R.id.currency_rate)

    private val regularFormat = getDecimalFormat(REGULAR_FORMAT_PATTERN)
    private val exponentialFormat = getDecimalFormat(EXPONENTIAL_FORMAT_PATTERN)
    private val imageSize = itemView.resources.getDimension(R.dimen.icon_view_size).toInt()

    fun bind(
        item: RateItem,
        rateMultiplier: Float,
        clickListener: RatesAdapterItemClickListener,
        textWatcher: TextWatcher
    ) {

        rate.removeTextChangedListener(textWatcher)

        code.text = item.code
        description.text = item.description

        setFormattedRate(item, rateMultiplier)

        Glide.with(itemView)
            .load(item.iconUrl)
            .placeholder(R.drawable.ic_attach_money_black_24dp)
            .transform(CircleCrop())
            .override(imageSize, imageSize)
            .into(icon)

        rate.setOnClickListener {
            updateItemListeners(clickListener, textWatcher)
        }
        rate.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                updateItemListeners(clickListener, textWatcher)
            }
        }
    }

    private fun getDecimalFormat(pattern: String): DecimalFormat {
        val symbols = DecimalFormatSymbols()
        symbols.decimalSeparator = DOT
        return DecimalFormat(pattern, symbols)
    }

    private fun updateItemListeners(
        clickListener: RatesAdapterItemClickListener,
        textWatcher: TextWatcher
    ) {
        if (adapterPosition == 0) {
            this.rate.addTextChangedListener(textWatcher)
        }
        clickListener.onItemClicked(adapterPosition)
    }

    fun updateRate(item: RateItem, multiplier: Float, textWatcher: TextWatcher) {
        rate.removeTextChangedListener(textWatcher)
        setFormattedRate(item, multiplier)
    }

    private fun setFormattedRate(item: RateItem, rateMultiplier: Float) {
        GlobalScope.launch(context = Main) {
            val someValue = withContext(Dispatchers.IO) {
                formatRate(item.rate * rateMultiplier)
            }
            rate.setText(someValue)
        }
    }

    private fun formatRate(rate: Float) = if (rate >= EXPONENTIAL_FORMAT_EDGE || rate == 0f) {
        regularFormat.format(rate)
    } else {
        exponentialFormat.format(rate)
    }

    fun updateFocus(hasFocus: Boolean, textWatcher: TextWatcher) {
        if (hasFocus) {
            rate.addTextChangedListener(textWatcher)
        } else {
            rate.removeTextChangedListener(textWatcher)
        }
    }
}