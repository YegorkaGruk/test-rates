package com.yegor.gruk.data.store.currency_names

import com.yegor.gruk.data.model.response.CurrencyNameResponse
import com.yegor.gruk.data.service.storage.AssetsService
import io.reactivex.Single
import javax.inject.Inject

class CurrencyNamesStoreImpl @Inject constructor(
    private val service: AssetsService
) : CurrencyNamesStore {

    override fun loadRates(): Single<CurrencyNameResponse> {
        return service.readCurrencyNames()
    }
}