package com.yegor.gruk.data.repository

import com.yegor.gruk.data.model.response.CurrencyNameItemResponse
import com.yegor.gruk.data.model.response.CurrencyNameResponse
import com.yegor.gruk.data.model.response.RatesResponse
import com.yegor.gruk.data.store.currency_image.CurrencyImageStore
import com.yegor.gruk.data.store.currency_names.CurrencyNamesStore
import com.yegor.gruk.data.store.rate_api.RateApiStore
import com.yegor.gruk.domain.entity.CurrencyEntity
import com.yegor.gruk.domain.entity.RateEntity
import com.yegor.gruk.domain.repository.CurrenciesRepository
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CurrenciesRepositoryImpl @Inject constructor(
    private val apiStore: RateApiStore,
    private val imageStore: CurrencyImageStore,
    private val namesStore: CurrencyNamesStore
) : CurrenciesRepository {

    companion object {
        private const val BASE_RATE = 1f
        private const val UPDATE_INTERVAL = 1L
        private const val NO_DESCRIPTION_TEXT = ""
    }

    override fun loadCurrencies(): Flowable<CurrencyEntity> {
        return apiStore.loadRates()
            .repeatWhen { it.delay(UPDATE_INTERVAL, TimeUnit.SECONDS) }
            .concatMap { rates ->
                namesStore.loadRates().toFlowable().map { transformRatesMap(rates, it) }
            }
    }

    private fun transformRatesMap(
        rates: RatesResponse,
        names: CurrencyNameResponse
    ): CurrencyEntity {
        val transformed = rates.rates.map { rateEntity(it.key, it.value, names.results) }

        val mutableList = mutableListOf<RateEntity>()
        mutableList.add(rateEntity(rates.base, BASE_RATE, names.results))
        mutableList.addAll(transformed)

        return CurrencyEntity(rates.base, rates.date, mutableList)
    }

    private fun rateEntity(
        code: String,
        rate: Float,
        results: Map<String, CurrencyNameItemResponse>
    ): RateEntity {
        val name = results[code]
        return RateEntity(
            code,
            rate,
            name?.currencyName ?: NO_DESCRIPTION_TEXT,
            imageStore.mapCodeToImageUri(code)
        )
    }
}