package com.yegor.gruk.data.service.storage

import android.content.Context
import com.google.gson.Gson
import com.yegor.gruk.data.CURRENCY_NAMES_FILE_NAME
import com.yegor.gruk.data.model.response.CurrencyNameResponse
import io.reactivex.Single
import javax.inject.Inject

class AssetsServiceImpl @Inject constructor(private val context: Context) : AssetsService {

    override fun readCurrencyNames(): Single<CurrencyNameResponse> {
        return Single.fromCallable { getAssetJsonData(context) }
            .map { Gson().fromJson(it, CurrencyNameResponse::class.java) }
    }

    private fun getAssetJsonData(context: Context): String {
        return context.assets
            .open(CURRENCY_NAMES_FILE_NAME)
            .bufferedReader()
            .use { it.readText() }
    }
}