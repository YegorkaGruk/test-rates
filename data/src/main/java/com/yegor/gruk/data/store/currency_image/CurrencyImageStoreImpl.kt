package com.yegor.gruk.data.store.currency_image

import com.yegor.gruk.data.CURRENCY_ICON_URI
import javax.inject.Inject

class CurrencyImageStoreImpl @Inject constructor() : CurrencyImageStore {

    override fun mapCodeToImageUri(code: String) = CURRENCY_ICON_URI.format(code.toLowerCase())
}