package com.yegor.gruk.data.store.rate_api

import com.yegor.gruk.data.model.response.RatesResponse
import com.yegor.gruk.data.service.rest.RatesService
import io.reactivex.Single
import javax.inject.Inject

class RateApiStoreImpl @Inject constructor(
    private val service: RatesService
) : RateApiStore {

    override fun loadRates(): Single<RatesResponse> {
        return service.loadCurrencies()
    }
}