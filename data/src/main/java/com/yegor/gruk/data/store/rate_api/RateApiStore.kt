package com.yegor.gruk.data.store.rate_api

import com.yegor.gruk.data.model.response.RatesResponse
import io.reactivex.Single

interface RateApiStore {
    fun loadRates(): Single<RatesResponse>
}