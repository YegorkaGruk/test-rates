package com.yegor.gruk.data.model.response

import com.google.gson.annotations.SerializedName

class RatesResponse(
    @SerializedName("base")
    val base: String,
    @SerializedName("date")
    val date: String,
    @SerializedName("rates")
    val rates: Map<String, Float>
)