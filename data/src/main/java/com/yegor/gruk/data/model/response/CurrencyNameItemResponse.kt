package com.yegor.gruk.data.model.response

import com.google.gson.annotations.SerializedName

data class CurrencyNameItemResponse(
    @SerializedName("currencyName")
    val currencyName: String,
    @SerializedName("currencySymbol")
    val currencySymbol: String?,
    @SerializedName("id")
    val id: String
)