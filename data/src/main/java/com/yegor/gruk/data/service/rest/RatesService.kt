package com.yegor.gruk.data.service.rest

import com.yegor.gruk.data.LATEST_PATH
import com.yegor.gruk.data.model.response.RatesResponse
import io.reactivex.Single
import retrofit2.http.GET

interface RatesService {
    @GET(LATEST_PATH)
    fun loadCurrencies(): Single<RatesResponse>
}