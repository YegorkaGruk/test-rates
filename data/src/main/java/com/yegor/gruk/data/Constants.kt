package com.yegor.gruk.data

const val BASE_URL = "https://revolut.duckdns.org"

const val LATEST_PATH = "latest"

const val CURRENCY_NAMES_FILE_NAME = "currency_names.json"

const val CURRENCY_ICON_URI =
    "https://raw.githubusercontent.com/cychiang/currency-icons/master/icons/currency/%s.png"