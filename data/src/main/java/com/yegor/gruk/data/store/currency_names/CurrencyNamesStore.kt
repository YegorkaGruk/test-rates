package com.yegor.gruk.data.store.currency_names

import com.yegor.gruk.data.model.response.CurrencyNameResponse
import io.reactivex.Single

interface CurrencyNamesStore {
    fun loadRates(): Single<CurrencyNameResponse>
}