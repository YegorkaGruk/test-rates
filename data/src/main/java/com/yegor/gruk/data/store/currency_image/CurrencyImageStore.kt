package com.yegor.gruk.data.store.currency_image

interface CurrencyImageStore {
    fun mapCodeToImageUri(code: String): String
}