package com.yegor.gruk.data.model.response

import com.google.gson.annotations.SerializedName

data class CurrencyNameResponse(
    @SerializedName("results")
    val results: Map<String, CurrencyNameItemResponse>
)