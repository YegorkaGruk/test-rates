package com.yegor.gruk.data.service.storage

import com.yegor.gruk.data.model.response.CurrencyNameResponse
import io.reactivex.Single

interface AssetsService {

    fun readCurrencyNames(): Single<CurrencyNameResponse>
}