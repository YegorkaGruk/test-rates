package com.yegor.gruk.domain.usecase.base

import androidx.annotation.Nullable
import io.reactivex.Completable

interface CompletableUseCase<Params> : BaseUseCase {
    fun execute(@Nullable params: Params?): Completable
}