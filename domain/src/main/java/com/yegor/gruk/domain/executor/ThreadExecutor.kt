package com.yegor.gruk.domain.executor

import io.reactivex.Scheduler

interface ThreadExecutor {
    fun provideScheduler(): Scheduler
}