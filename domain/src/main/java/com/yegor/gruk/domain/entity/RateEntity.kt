package com.yegor.gruk.domain.entity

data class RateEntity(
    val code: String,
    val rate: Float,
    val description: String,
    val iconUri: String
)