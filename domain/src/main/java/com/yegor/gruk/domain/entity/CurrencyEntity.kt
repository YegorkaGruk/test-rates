package com.yegor.gruk.domain.entity

data class CurrencyEntity(
    val base: String,
    val date: String,
    val rates: List<RateEntity>
)