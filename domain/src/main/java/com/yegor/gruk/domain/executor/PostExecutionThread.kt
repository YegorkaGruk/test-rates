package com.yegor.gruk.domain.executor

import io.reactivex.Scheduler

interface PostExecutionThread {
    fun provideScheduler(): Scheduler
}