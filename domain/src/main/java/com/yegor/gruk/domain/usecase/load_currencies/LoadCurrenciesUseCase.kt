package com.yegor.gruk.domain.usecase.load_currencies

import com.yegor.gruk.domain.entity.CurrencyEntity
import com.yegor.gruk.domain.executor.PostExecutionThread
import com.yegor.gruk.domain.executor.ThreadExecutor
import com.yegor.gruk.domain.repository.CurrenciesRepository
import com.yegor.gruk.domain.usecase.base.FlowableUseCase
import io.reactivex.Flowable
import io.reactivex.Scheduler
import javax.inject.Inject

class LoadCurrenciesUseCase @Inject constructor(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread,
    private val repository: CurrenciesRepository
) : FlowableUseCase<CurrencyEntity, Void?> {

    private val threadExecutorScheduler: Scheduler = threadExecutor.provideScheduler()
    private val postExecutionThreadScheduler: Scheduler = postExecutionThread.provideScheduler()

    override fun execute(request: Void?): Flowable<CurrencyEntity> {
        return repository.loadCurrencies()
            .subscribeOn(threadExecutorScheduler)
            .observeOn(postExecutionThreadScheduler)
    }
}
