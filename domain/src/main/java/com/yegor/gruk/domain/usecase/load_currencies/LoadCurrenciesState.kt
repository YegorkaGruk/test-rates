package com.yegor.gruk.domain.usecase.load_currencies

import com.yegor.gruk.domain.entity.CurrencyEntity

sealed class LoadCurrenciesState {

    object StateProcessing : LoadCurrenciesState()

    data class StateSuccess(val result: CurrencyEntity) : LoadCurrenciesState()

    data class StateError(val error: Throwable) : LoadCurrenciesState()
}