package com.yegor.gruk.domain.repository

import com.yegor.gruk.domain.entity.CurrencyEntity
import io.reactivex.Flowable

interface CurrenciesRepository {
    fun loadCurrencies(): Flowable<CurrencyEntity>
}