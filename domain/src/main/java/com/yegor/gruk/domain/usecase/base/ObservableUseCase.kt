package com.yegor.gruk.domain.usecase.base

import androidx.annotation.Nullable
import io.reactivex.Observable

interface ObservableUseCase<Results, Params> : BaseUseCase {
    fun execute(@Nullable params: Params?): Observable<Results>
}