package com.yegor.gruk.domain.usecase.base

import com.yegor.gruk.domain.executor.PostExecutionThread
import com.yegor.gruk.domain.executor.ThreadExecutor
import io.reactivex.Scheduler

abstract class BaseRepositoryUseCase(
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) {

    protected val threadExecutorScheduler: Scheduler
    protected val postExecutionThreadScheduler: Scheduler

    init {
        threadExecutorScheduler = threadExecutor.provideScheduler()
        postExecutionThreadScheduler = postExecutionThread.provideScheduler()
    }
}