package com.yegor.gruk.domain.exception

import java.io.IOException

class NoConnectionException : IOException {

    constructor() : super(MESSAGE)

    constructor(throwable: Throwable) : super(MESSAGE, throwable)

    companion object {
        private const val MESSAGE = "Unable to connect to server"
    }
}