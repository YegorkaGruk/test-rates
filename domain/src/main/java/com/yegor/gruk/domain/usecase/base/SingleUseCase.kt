package com.yegor.gruk.domain.usecase.base

import androidx.annotation.Nullable
import io.reactivex.Single

interface SingleUseCase<Results, Params> : BaseUseCase {
    fun execute(@Nullable params: Params?): Single<Results>
}